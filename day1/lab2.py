def wordcounts(pth):
    f = open(pth)
    d = {}

    for line in f:
        l = line.split()

        for word in l:
            if word in d:
                d[word] += 1
            else:
                d[word] = 1

    return d

    # todo: print words in descending order of frequency
    pass

print(wordcounts('corpus.txt'))